<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UtilTest extends TestCase
{
    public function testUser(): void
    {
        $user = new User();
        $user->setNom('RAZAFIMANDIMBY');
        $user->setPrenom('Marcel');

        $this->assertTrue($user->getNom() === "RAZAFIMANDIMBY");
    }
}
